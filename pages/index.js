import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { Row, Col } from 'antd';
import HeaderCust from './_header';
import FooterCust from './_footer';
import { NAME, SKILLS } from '../lib/_constants';
import '../custom.less';

import Intro from './_intro';

const { Content } = Layout;

const Home = () => {
  return (
    <Layout>
      <HeaderCust selected={'0'} />
      <Content>
        <Row style={{ height: '90vh' }}>
          <Col span={24} style={{ textAlign: 'center' }}>
            <div style={{ paddingTop: '40vh', width: '100%' }}>
              <Intro name={NAME} skills={SKILLS} />
            </div>
          </Col>
        </Row>
      </Content>
      <Row style={{ height: '10vh' }}>
        <FooterCust />
      </Row>
    </Layout>
  );
};

export default Home;
