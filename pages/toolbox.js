import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import { Tabs } from "antd";
import { Row, Col, Input, Button } from "antd";
import HeaderCust from "./_header";
import FooterCust from "./_footer";
import IamToHCLConverter from "../lib/IamToHCLConverter";
import KFCCode from "../lib/KFCCode";
import "../custom.less";

const { TabPane } = Tabs;
const { Header, Footer, Sider, Content } = Layout;
const { TextArea } = Input;

export const Toolbox = () => {
  return (
    <Layout>
      <HeaderCust selected={"2"} />
      <Content style={{marginTop: "8vh"}}>
        <Tabs defaultActiveKey="0">
          <TabPane tab="IAM Policy JSON to HCL" key="0">
            <IamToHCLConverter/>
          </TabPane>
          <TabPane tab="KFCCode NZ" key="1">
            <KFCCode/>
          </TabPane>
        </Tabs>
      </Content>
      <Row style={{ height: "10vh" }}>
        <FooterCust />
      </Row>
    </Layout>
  );
};


export default Toolbox;
