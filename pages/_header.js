import React from 'react';
import { Layout, Menu } from 'antd';
import Link from 'next/link';
import Head from 'next/head'
import '../custom.less';

const { Header } = Layout;

const HeaderCust = ({ selected }) => {
  // Inject the header content here
  return (
    <Header className={'header'}>
      <div>
        <Head>
          <title>Edward Huang|Developer</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" key="html-head" />
        </Head>
      </div>
      <Menu
        theme={'dark'}
        mode="horizontal"
        style={{ lineHeight: '64px' }}
        defaultSelectedKeys={[selected]}
      >
        <Menu.Item key="0" className={'button'}>
          <Link href="/">
            <a>Home</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="1" className={'button'}>
          <Link href="/about">
            <a>About</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="2" className={'button'}>
          <Link href="/toolbox">
            <a>Toolbox</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="3" className={'button'}>
          <a href={"https://blog.edwardhuang.nz"}>Blog</a>
        </Menu.Item>
      </Menu>
    </Header>
  );
};

export default HeaderCust;
