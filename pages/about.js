import React from 'react';
import { Layout, Menu, Button, Icon } from 'antd';
import { Row, Col } from 'antd';
import '../custom.less';
import HeaderCust from './_header';
import FooterCust from './_footer';

const { Content } = Layout;

const FillUpSpan = ({ text }) => {
  return <span className={'fill fill-up'}>{text}</span>;
};

const FillDownSpan = ({ text }) => {
  return <span className={'fill fill-down'}>{text}</span>;
};

const mkFillSpan = (text, orientation = 'up') => {
  if (orientation === 'up') {
    return <FillUpSpan text={text} />;
  } else if (orientation === 'down') {
    return <FillDownSpan text={text} />;
  }
};

const Contact = () => {
  return (
    <Layout>
      <HeaderCust selected={'1'} />
      <Content>
        <Row style={{ height: '90vh' }}>
          <Col span={24} style={{ marginTop: '30vh', textAlign: 'center'}}>
            <h2>Hi, my name is Edward Huang.</h2>
            <h3>
              I've developed {mkFillSpan('Wireshark')} with {mkFillSpan('C')},
              made hobby projects with {mkFillSpan('Scala')},{' '}
              {mkFillSpan('Python')} and {mkFillSpan('Haskell')}.
            </h3>
            <h3>
              I've also made front-ends with {mkFillSpan('React + Redux')} and{' '}
              {mkFillSpan('Elm')}.
            </h3>
            <h3>
              I use {mkFillSpan('Docker')} and {mkFillSpan('gitlab-ci')} to maintain this website and my blog.
            </h3>
            <h3>
            Recently, I have been dabbling with {mkFillSpan('Rust')} and {mkFillSpan('Android Development')}.
            </h3>
            <h3>
              You can find my publications <a href={'https://ieeexplore.ieee.org/author/37086827186'}>here</a>
            </h3>
          </Col>
          <Col span={24} style={{ textAlign: 'center', paddingTop: '0%' }}>
            <Button
              shape={'round'}
              icon={'mail'}
              type={'primary'}
              size={'large'}
              href={'mailto:hello@edwardhuang.nz'}
            >
              Shoot me an Email
            </Button>
          </Col>
        </Row>
        <Row>
          <Col span={12} style={{ textAlign: 'center'}}>
            <Button
              shape={'round'}
              icon={'key'}
              type={'primary'}
              size={'large'}
              href={'/sshpub.txt'}
            >
              SSH Key
            </Button>
          </Col>
          <Col span={12} style={{ textAlign: 'center'}}>
            <Button
              shape={'round'}
              icon={'key'}
              type={'primary'}
              size={'large'}
              href={'/gpg.txt'}
            >
              GPG Key
            </Button>
          </Col>
        </Row>
      </Content>
      <Row style={{ height: '10vh' }}>
        <FooterCust />
      </Row>
    </Layout>
  );
};

export default Contact;
