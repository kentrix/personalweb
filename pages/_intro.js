import React from 'react';
import ReactRotatingText from 'react-rotating-text';
import PropTypes from 'prop-types';

const Intro = ({ name, skills }) => {
  return (
    <div>
      <h2 className={'intro-heading'}>{name}</h2>
      <ReactRotatingText items={skills} className={'intro-rotating-text'} />
    </div>
  );
};

Intro.propTypes = {
  name: PropTypes.string.isRequired,
  skills: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Intro;
