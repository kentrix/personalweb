import React from "react";
import { Row, Col, Button, InputNumber, Alert, Input } from "antd";
import Converter from "iam-hcl-converter";
import "../custom.less";



class KFCCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      rawCode: "",
      endCode: "",
      error: null,
    };
    this.setRawCodeText = this.setRawCodeText_.bind(this);
    this.doGetCode = this.doGetCode_.bind(this);
    this.setEndCodeText = this.setEndCodeText_.bind(this);
    this.setError = this.setError_.bind(this);
    this.setLoading = this.setLoading_.bind(this);
  }

  setRawCodeText_({ target: { value } }) {
    this.setState({ rawCode: value });
  }

  setEndCodeText_(value) {
    this.setState({ endCode: value });
  }

  setError_(err) {
    this.setState({ error: err });
  }

  setLoading_(loading) {
    this.setState({ loading });
  }

  doGetCode_() {
    this.setLoading(true);
    this.setError(null);
    fetch(`https://kfccode.edwardhuang.nz/${this.state.rawCode}/`).then((resp) => {
      if (resp.ok) {
        this.setEndCodeText(resp.body.json());
      } else {
        this.setError('Upstream server query failed! (I probably turned the server off lol)')
      }
    }).catch((why) => {
      this.setError(why);
    }).finally(() => {
      this.setLoading(false);
    })
  }

  render() {
    return (
      <div>
        <Row>
          <Col span={24} style={{ textAlign: "center", height: "100%" }}>
            <Input onChange={this.setRawCodeText} placeholder="Enter your 16 digit code here"></Input>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            <Button onClick={this.doGetCode} loading={this.state.loading} disabled={this.state.loading}>Get</Button>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            {this.renderOutput()}
          </Col>
        </Row>
      </div>
    );
  }

  renderOutput() {
    const errText = `Error + ${this.state.error}`;
    if (this.state.error) {
      return (
        <Alert message={errText} type="error"></Alert>
      )
    } else if (this.state.endCode) {
      return (
        <Alert message={`Code: ${this.state.endCode}`} type="message"></Alert>
      )
    }
  }
}

export default KFCCode;
