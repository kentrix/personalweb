import React from "react";
import { Row, Col, Button, Input } from "antd";
import Converter from "iam-hcl-converter";
import "../custom.less";

const { TextArea } = Input;

// A stateful component because I dont want to deal with redux for now
class IamToHCLConverter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
      outputText: "",
      error: undefined
    };
    this.setInputText = this.setInputText_.bind(this);
    this.setOutputText = this.setOutputText_.bind(this);
    this.setError = this.setError_.bind(this);
    this.doConvert = this.doConvert_.bind(this);
    this.onInputChange = this.onInputChange_.bind(this);
  }

  onInputChange_({ target: { value } }) {
    this.setInputText(value);
  }

  setInputText_(v) {
    this.setState({ inputText: v });
  }

  setOutputText_(v) {
    this.setState({ outputText: v });
  }

  setError_(v) {
    this.setState({ error: v });
  }

  doConvert_() {
    try {
      const conv = new Converter(2);
      const out = conv.convert(this.state.inputText);
      this.setOutputText(out);
      this.setError(undefined);
    } catch (e) {
      this.setError(e);
      this.setOutputText('');
    }
  }

  render() {
    return (
      <div>
        <Row style={{ height: "30vh" }}>
          <Col span={24} style={{ textAlign: "center", height: "100%" }}>
            <TextArea
              style={{ height: "100%" }}
              id="json-input"
              onChange={this.onInputChange}
              onPressEnter={this.doConvert}
              placeholder={"Put IAM JSON policy here"}
            ></TextArea>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            <Button onClick={this.doConvert}>Convert</Button>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            <TextArea
              id="json-output"
              autoSize={true}
              value={this.state.outputText}
            ></TextArea>
          </Col>
        </Row>
        {this.state.error === undefined ? "" : renderErrorField(this.state.error)}
      </div>
    );
  }
}

const renderErrorField = (err) => {
  return (
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            <TextArea
              id="error"
              autoSize={true}
              disabled={true}
              value={err.toString()}
            ></TextArea>
          </Col>
        </Row>
)};

export default IamToHCLConverter;