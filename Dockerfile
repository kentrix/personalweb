FROM node:16
COPY . /server/
WORKDIR /server
RUN ["npm", "install"]
RUN ["npm", "run", "build"]
CMD ["npm", "run", "start"]
EXPOSE 3000
